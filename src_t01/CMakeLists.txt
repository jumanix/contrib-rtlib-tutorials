
#----- Check for the required RTLib library
find_package(BbqRTLib REQUIRED)

#----- Add compilation dependencies
include_directories(${BBQUE_RTLIB_INCLUDE_DIR})

#----- Add "BbqRTLibTutorial01" target application
set(RTLIB_TUTORIAL_SRC tutorial01 app)
add_executable(BbqRTLibTutorial01 ${RTLIB_TUTORIAL_SRC})

#----- Linking dependencies
target_link_libraries(
	BbqRTLibTutorial01
	${BBQUE_RTLIB_LIBRARY}
)

# Use link path ad RPATH
set_property(TARGET BbqRTLibTutorial01 PROPERTY
	INSTALL_RPATH_USE_LINK_PATH TRUE)

#----- Install the Tutorial files
install (TARGETS BbqRTLibTutorial01 RUNTIME
	DESTINATION ${RTLIB_TUTORIAL_PATH_BINS}
	COMPONENT Tutorial01)
install (FILES "./rtlib_tutorial01.txt"
	DESTINATION ${RTLIB_TUTORIAL_PATH_BINS}
	COMPONENT Tutorial01)

